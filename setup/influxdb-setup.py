# coding=utf-8
import requests
import pathlib
import json

if __name__ == '__main__':
    fp               = pathlib.Path('influxdb-config.json')
    conf             = json.loads(fp.read_text())
    influxdb_host    = conf['server_url']
    influxdb_root    = conf['user_admin']
    influxdb_dbs     = conf['databases']
    influxdb_readers = conf['users_reader'] # DBs = [] for all databases, else only ones in the list
    influxdb_writers = conf['users_writer'] # DBs = [] for all databases, else only ones in the list

    url = '{:s}/query'.format(influxdb_host)
    query = dict()
    query['u'] = influxdb_root['login']
    query['p'] = influxdb_root['passw']

    # ---------- Influx : Creation of admin if needed ----------
    print("\nInfluxDB : Creation of Admin {:s}".format(influxdb_root['login']))
    query['q'] = "CREATE USER {:s} WITH PASSWORD '{:s}' WITH ALL PRIVILEGES".format(influxdb_root['login'], influxdb_root['passw'])
    response = requests.request("POST", url, params=query)
    print('  -> {:s}'.format(str(response.json())))

    # ---------- Influx : Databases ----------
    print("\nInfluxDB : Creation of Databases")
    for infdb in sorted(influxdb_dbs.keys()) :
        query['q'] = 'CREATE DATABASE "{:s}" WITH DURATION {:s} NAME "retention{:s}"'.format(infdb, influxdb_dbs[infdb], influxdb_dbs[infdb])
        response = requests.request("POST", url, params=query)
        print('  - {:s} -> {:s}'.format(infdb, str(response.json())))
    query['q'] = 'show databases'
    response = requests.request("POST", url, params=query)
    list_dbs = list()
    for sublist in response.json()['results'][0]['series'][0]['values'] :
        list_dbs.append(sublist[0])
    print("InfluxDB : Databases = {:s}".format(str(list_dbs)))
    list_dbs.remove('_internal')

    # ---------- Influx : Creation of users ----------
    print("\nInfluxDB : Creation of Readers")
    for uread in influxdb_readers :
        query['q'] = "CREATE USER {:s} WITH PASSWORD '{:s}'".format(uread['login'], uread['passw'])
        response = requests.request("POST", url, params=query)
        print('  - {:s} -> {:s}'.format(uread['login'], str(response.json())))
        liste_db = uread['DBs'] if len(uread['DBs'])>0 else list_dbs
        for infdb in liste_db :
            query['q'] = 'GRANT READ ON "{:s}" TO "{:s}"'.format(infdb, uread['login'])
            response = requests.request("POST", url, params=query)
            print('  - {:s} READ ON {:s} -> {:s}'.format(uread['login'], infdb, str(response.json())))

    print("\nInfluxDB : Creation of WRITERS")
    for uread in influxdb_writers :
        query['q'] = "CREATE USER {:s} WITH PASSWORD '{:s}'".format(uread['login'], uread['passw'])
        response = requests.request("POST", url, params=query)
        print('  - {:s} -> {:s}'.format(uread['login'], str(response.json())))
        liste_db = uread['DBs'] if len(uread['DBs'])>0 else list_dbs
        for infdb in liste_db :
            query['q'] = 'GRANT WRITE ON "{:s}" TO "{:s}"'.format(infdb, uread['login'])
            response = requests.request("POST", url, params=query)
            print('  - {:s} WRITE ON {:s} -> {:s}'.format(uread['login'], infdb, str(response.json())))
    print("\n")
